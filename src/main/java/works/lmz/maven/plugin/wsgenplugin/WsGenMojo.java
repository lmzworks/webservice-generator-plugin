package works.lmz.maven.plugin.wsgenplugin;

import java.io.File;
import java.io.FileFilter;
import java.io.FileWriter;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.nio.channels.FileChannel;
import java.util.ArrayList;
import java.util.List;

import org.apache.cxf.tools.wsdlto.WSDLToJava;
import org.apache.maven.plugin.AbstractMojo;
import org.apache.maven.plugin.MojoExecutionException;

/**
 * Generates web service source from WSDL.
 *
 * @goal generate
 * @phase generate-sources
 */
public class WsGenMojo extends AbstractMojo {

	/**
	 * Location of the project target.
	 *
	 * @parameter expression="${project.build.directory}"
	 * @required
	 */
	private File buildDir;

	/**
	 * @parameter expression="${basedir}"
	 * @required
	 */
	private File baseDir;

	/**
	 * @parameter expression="${project.groupId}"
	 * @required
	 */
	private String projectGroupId;

	/**
	 * @parameter expression="${project.artifactId}"
	 * @required
	 */
	private String projectArtifactId;

	/**
	 * The catalog file writer.
	 */
	private FileWriter catalogWriter;

	/**
	 * Supports mapping the generated code into a package or packages.
	 *
	 * @parameter
	 */
	private List<String> packageMapping;

	/**
	 * Optionally define the specific wsdl file to generate.
	 *
	 * @parameter
	 */
	private String wsdl;

	/**
	 * Return the source directory that holds the wsdl, xsd and binding files.
	 */
	private File getJaxWsSourceDir() {
		return new File(baseDir, "/src/main/jax-ws");
	}

	/**
	 * Return the destination directory for the generated source.
	 */
	private File getGeneratedSourceDir() {
		return new File(buildDir, "/generated-sources/jax-ws");
	}

	/**
	 * Return the destination directory for the resources.
	 */
	private File getGeneratedResourcesDir() {
		return new File(buildDir, "/generated-sources/jax-ws-resources");
	}

	private String getFakeUrl() {
		return "http://jax-ws-catalog/" + projectGroupId + "/" + projectArtifactId;
	}

	/**
	 * Execute generating the web service source code and moving resources files
	 * as necessary.
	 */
	public void execute() throws MojoExecutionException {

		try {
			ensureDirectories();

			copyFiles();

			startCatalog();

			File[] wsdls = findWsdls();
			for (File wsdlFile : wsdls) {
				processWsdl(wsdlFile);
			}

			endCatalog();

		} catch (Exception e) {
			getLog().error("Unexpected failure", e);
			throw new MojoExecutionException("Unexpected failure", e);
		}

	}

	/**
	 * Copy the wsdl, xsd and xml files into the destination resources directory.
	 */
	protected void copyFiles() throws IOException {

		File destDir = new File(getGeneratedResourcesDir(), "/META-INF/wsdl/");
		destDir.mkdirs();

		File jaxWsDir = getJaxWsSourceDir();
		File[] copyList = jaxWsDir.listFiles(new CopyFilter());

		for (File fromFile : copyList) {
			File toFile = new File(destDir, fromFile.getName());
			copyFile(fromFile, toFile);
		}
	}

	/**
	 * Start the catalog file.
	 */
	private void startCatalog() throws IOException {

		File catalogFile = new File(getGeneratedResourcesDir(), "/META-INF/jax-ws-catalog.xml");
		catalogFile.getParentFile().mkdirs();

		catalogWriter = new FileWriter(catalogFile);
		catalogWriter.write("<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n");
		catalogWriter.write("<catalog xmlns=\"urn:oasis:names:tc:entity:xmlns:xml:catalog\">\n");
	}

	/**
	 * Add an entry into the catalog file.
	 */
	private void addCatalogEntry(String fakePrefix, String wsdl) throws IOException {
		catalogWriter.write(" <system systemId=\"" + fakePrefix + "/" + wsdl + "\" uri=\"wsdl/" + wsdl + "\" />\n");
	}

	/**
	 * End the catalog file.
	 */
	private void endCatalog() throws IOException {
		catalogWriter.write("</catalog>\n");
		catalogWriter.flush();
		catalogWriter.close();
	}

	/**
	 * Process a wsdl generating the source and adding an entry into the catalog
	 * file.
	 */
	private void processWsdl(File wsdlFile) throws IOException {

		if (!wsdlFile.exists()) {
			throw new RuntimeException("No WSDL File " + wsdlFile.toString() + " ?");
		}

		getLog().info("Generating web service source for: " + wsdlFile);

		File generatedSourceDir = getGeneratedSourceDir();

		// build the argument list for WSDLToJava
		List<String> argumentList = new ArrayList<String>();

		// directory for the generated source
		argumentList.add("-d");
		argumentList.add(generatedSourceDir.toString());

		if (packageMapping != null) {
			// mapping generated source to a package or packages
			for (String packageNameMapping : packageMapping) {
				argumentList.add("-p");
				argumentList.add(packageNameMapping);
			}
		}

		//TODO add support for binding files

		argumentList.add("-validate");
		argumentList.add("-autoNameResolution");
		argumentList.add("-wsdlLocation");
		argumentList.add(getFakeUrl());
		argumentList.add(wsdlFile.toString());

		String[] args = argumentList.toArray(new String[argumentList.size()]);
		WSDLToJava.main(args);

		// add the web service to the catalog xml file
		addCatalogEntry(getFakeUrl(), wsdlFile.getName());
	}

	/**
	 * Return the wsdl files.
	 */
	protected File[] findWsdls() {

		File jaxWsDir = getJaxWsSourceDir();
		if (wsdl != null) {
			// explicitly specified the wsdl to generate
			File[] wsdlFiles = new File[1];
			wsdlFiles[0] = new File(jaxWsDir, wsdl);
			return wsdlFiles;
		}

		// search for the wsdl
		return jaxWsDir.listFiles(new WsdlFilter());
	}

	/**
	 * Copy the file from source to destination.
	 */
	private void copyFile(File from, File to) throws IOException {

		RandomAccessFile fromFile = new RandomAccessFile(from, "r");
		FileChannel fromChannel = fromFile.getChannel();

		RandomAccessFile toFile = new RandomAccessFile(to, "rw");
		FileChannel toChannel = toFile.getChannel();

		long position = 0;
		long count = fromChannel.size();

		fromChannel.transferTo(position, count, toChannel);
	}

	/**
	 * Ensure the directories are created.
	 */
	private void ensureDirectories() {

		if (!buildDir.exists()) {
			buildDir.mkdirs();
		}

		File generatedSourceDir = getGeneratedSourceDir();
		File generatedResourcesDir = getGeneratedResourcesDir();

		generatedSourceDir.mkdirs();
		generatedResourcesDir.mkdirs();
	}

	/**
	 * Filter just the wsdl files.
	 */
	private static class WsdlFilter implements FileFilter {
		public boolean accept(File pathname) {
			return pathname.getName().toLowerCase().endsWith(".wsdl");
		}
	}

	/**
	 * Filter for wsdls, xsd and xml files.
	 */
	private static class CopyFilter implements FileFilter {
		public boolean accept(File pathname) {
			String lowerPath = pathname.getName().toLowerCase();
			return lowerPath.endsWith(".wsdl") || lowerPath.endsWith(".xml") || lowerPath.endsWith(".xsd");
		}
	}

}
